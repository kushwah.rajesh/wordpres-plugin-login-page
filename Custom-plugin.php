<?php

    /*
        plugin name: custom-plugin
        author:rajesh
        version: 1.0
        Description: This is a custom plugin
        plugin url: https://
    */

    define("PLUGIN_DIR_PATH",plugin_dir_path(__FILE__));
    define("PLUGIN_URL",plugins_url());
    define("PLUGIN_VERSION","1.0");


    function add_my_custom_menu(){
        add_menu_page("customplugin",
        "Custom Plugin",
        "manage_options",
        "custom Slug",
        "",
        "",
        25
    );

        add_submenu_page(
            "custom Slug",
            "Add new",
            "Add new",
            "manage_options",
            "custom Slug",
            "add_new_function"
    );
        add_submenu_page(
            "custom Slug",
            "all pages",
            "all pages",
            "manage_options",
            "all pages",
            "add_new_function2"
    );
}
    add_action("admin_menu","add_my_custom_menu");

    function add_new_function(){
        include_once PLUGIN_DIR_PATH."/views/add_new.php";
    }
    function add_new_function2(){
        echo "hello user";
    }

    function custom_plugin_assets(){
        
        wp_enqueue_script(
            "cpl_script",
            PLUGIN_URL."/Custom-plugin/views/script.js",
            "",
            PLUGIN_VERSION,
            false
        );
        
        
        wp_localize_script("cpl_script","ajaxurl",admin_url("admin-ajax.php"));
    }
    add_action("init","custom_plugin_assets");

    if(isset($_REQUEST['action'])){
        switch($_REQUEST['action']){
            case "custom_plugin_library": 
                add_action("admin_init","add_custom_plugin_library"); 
                    function add_custom_plugin_library(){
                        global $wpdb;
                        include_once PLUGIN_URL_PATH."/library/add-new.php";
                    }
                break;
        }
    }
  //  register_activation_hook( __FILE__, 'myPluginCreateTable');
    
    function myPluginCreateTable() {
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();
            $table_name = $wpdb->prefix . 'customTable';
            $sql = "CREATE TABLE `$table_name` (
            `id` int(11) NOT NULL,
            `Name` varchar(220) DEFAULT NULL,
            `Email` varchar(220) DEFAULT NULL,
            `Phone` int(11) DEFAULT '1',
            PRIMARY KEY(id)
            )ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

    //register_deactivation_hook(__FILE__,'myPluginDropTable');

    function myPluginDropTable(){
        global $wpdb;
        $wpdb->query("Drop table if exists wp_customTable");
    }


    register_activation_hook(__FILE__,'createNewPage');

    function createNewPage(){
        $page = Array();
        $page['post_title'] = "custom page creation";
        $page['post_Content'] = "This is a custom page created for practise";
        $page['post_status'] = "publish";
        $page['post_slug'] = "custom-plugin-page";
        $page['post_type'] = "page";
        $page['post_name'] = "custom creation";

        wp_insert_post($page);

    }

    register_deactivation_hook(__FILE__, 'deletepage_deactivation_function');

// callback function to drop table
function deletepage_deactivation_function()
{
    global $wpdb;

    $wpdb->query("DELETE FROM " . $wpdb->prefix . "posts WHERE post_title = 'custom page creation'");
}


?>  
